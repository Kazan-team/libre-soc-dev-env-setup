# libre-soc-dev-env-setup

This repository contains scripts that automate parts of the Libre-SOC development environment setup process, specifically the HDL Workflow (https://libre-riscv.org/HDL_workflow).